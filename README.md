# Zap-Map Technical Challenge

Hello! Here at Zap-Map, we love a good API. Your challenge is to develop a single restful API endpoint which should return Location points for a region on a map.

You should program this in PHP and MySQL (bonus points if you use Laravel), but we don’t want you to spend too long on it, only an hour or two, your time is precious. It’s fine if you only complete half of it or it errors somewhere, what really matters is that we can get a feel for how you would go about this challenge and how you would structure your code.

* The data for this challenge is in `data.csv`.
* Create a database and migrate the data from that file.
* There only needs to be a single endpoint.
* That endpoint should accept three parameters:
	* latitude
	* longitude
	* radius
* It should return a response of all Locations that fall within that radius.

If you have any questions, then please do contact us—we are a friendly bunch and want you to succeed! Once the challenge has been completed (or not, as might very well be the case…) then please send it across to us with any instructions that you think we might need.

Happy coding!
